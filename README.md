## __List of Demos:__
1. Use Docker for local development
2. Docker Compose - Run multiple Docker containers 
3. Dockerize Nodej sapplication and push to private Docker registry
4. Deploy Docker application on a server with Docker Compose
5. Persist data with Docker Volumes
6. Create Docker repository on Nexus and push to it
7. Deploy Nexus as Docker container