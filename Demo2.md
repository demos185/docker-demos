## __Demo Project:__
Docker Compose - Run multiple Docker containers 
## __Technologies used:__
Docker, MongoDB, MongoExpress
## __Project Decription:__
* Write Docker Compose file to run MongoDB and MongoExpress containers
## __Description in details:__

__Step 1:__ write `docker-compose.yaml`

#### To start the application

__Step 2:__ start mongodb and mongo-express
```sh
docker-compose -f docker-compose.yaml up
```    
_You can access the mongo-express under localhost:8080 from your browser_
    
__Step 3:__ in mongo-express UI - create a new database "my-db"

__Step 4:__ in mongo-express UI - create a new collection "users" in the database "my-db"       
    
__Step 5:__ start node server 
```sh
cd app
npm install
node server.js
```    
__Step 6:__ access the nodejs application from browser http://localhost:3000

#### To build a docker image from the application
```sh
docker build -t my-app:1.0 .       
```    
_Note: The dot "." at the end of the command denotes location of the Dockerfile._
