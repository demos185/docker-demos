## __Demo Project:__
Deploy Docker application on a server with Docker Compose
## __Technologies used:__
Docker, AmazonECR, Node.js, MongoDB, MongoExpress
## __Project Decription:__
* Copy Docker-compose file to remote server
* Configure your Docker-compose file and copy it to remote server
* Start our application container with MongoDB and MongoExpress services using dockercompose
## __Description in details:__
### __Configure your Docker-compose file and copy it to remote server__
__Step 1:__ Open Docker-compose file with your text editor and add new container with your image (configure this block of code):
```yaml
 my-app:
   image: ${docker-registry}/my-app:1.0
   ports:
    - 3000:3000
```
__Step 2:__ Copy Docker-compose file to  server
### __Login to private Docker registry on remote server to fetch our app image__
__Step 1:__ Execute docker login command (take this command from UI)

### __Start our application container with MongoDB and MongoExpress services using dockercompose__
__Step 1:__ Run docker-compose file `docker-compose -f mongo.yaml up `
