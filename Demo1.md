## __Demo Project:__
Use Docker for local development  
## __Technologies used:__
Docker, Node.js, MongoDB, MongoExpress
## __Project Decription:__
* Create Dockerfile for Nodejs application and build Docker image then run it
* Also run MongoExpress container asa UI of the MongoDB database
## __Description in details:__
### __Create Dockerfile for Nodejs application and build Docker image__
__Step 1:__ Create Dockerfile

__Step 2:__ Build imape with tag my-app version:1.0
```sh
docker build -t my-app:1.0
```
__Step 3:__ Run Nodejs Application
```sh
docker run my-app:1.0
```
_NOTE: When You __adjust__ the Dockerfile, you __MUST__  rebuild the image!_

### __Connect to MongoDB database container locally and run MongoExpress container asa UI of the MongoDB database__
__Step 1:__ Create docker network
```sh
docker network create mongo-network 
```
__Step 2:__ start mongodb 
```sh
docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password --name mongodb --net mongo-network mongo    
```
__Step 3:__ start mongo-express
```sh    
docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password --net mongo-network --name mongo-express -e ME_CONFIG_MONGODB_SERVER=mongodb mongo-express   
```
_NOTE: creating docker-network is optional. You can start both containers in a default network. In this case, just emit `--net` flag in `docker run` command_

__Step 4:__ open mongo-express from browser http://localhost:8081

__Step 5:__ create `user-account` _db_ and `users` _collection_ in mongo-express

__Step 6:__ Start your nodejs application locally - go to `app` directory of project 
```sh
cd app
npm install 
node server.js
```    
Step 7: Access you nodejs application UI from browser http://localhost:3000


