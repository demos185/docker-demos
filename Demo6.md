## __Demo Project:__
Create Docker repository on Nexus and push to it
## __Technologies used:__
Docker, Nexus, DigitalOcean, Linux
## __ProjectD ecription:__
* Create Docker hosted repository on Nexus
* Create Docker repository role on Nexus
* Configure Nexus, DigitalOcean Droplet and Docker to be able to push to Docker repository
* Build and Push Docker image to Docker repository on Nexus
## __Description in details:__
### __Create Docker hosted repository on Nexus__
__Step 1:__ Go to your nexus repository and create `docker(hosted)` repository and assign blob store
### __Create Docker repository role on Nexus__
__Step 1:__ Create docker role for Nexus with `nx-repository-view-docker-docker-hosted-*`

__Step 2:__ Assign role to your existing user 
### __Configure Nexus, DigitalOcean Droplet and Docker to be able to push to Docker repository__
__Step 1:__ Go to docker hosted repository edit __HTTP/HTTS__ field and assign port which is different from a Nexus port  e.g. `8083`

__Step 2:__ Open this port in DigitalOcean on droplet firewall configuration

__Step 3:__ Go to Nexus and activate `Docker bearer Token Relm` in `Security/Realms`

__Step 4:__ Configure Docker client to allow our hosted repository as an insecure registry edit `/etc/docker/daemon.json`:
```json
{
    "insecure-registries" : ["myregistrydomain.com:5000"]
}
```

__Step 5:__ Execure docker login
```sh
docker login myregistrydomain.com:5000
```
### __Build and Push Docker image to Docker repository on Nexus__

__Step 1:__ Build image
```sh
docker build -t my-image:1.0 .
```
__Step 2:__ Assign tag to image
```sh
docker tag  my-image:1.0 myregistrydomain.com:5000/my-image:1.0
```
__Step 3:__ Push our image to Nexus
```sh
docker push myregistrydomain.com:5000/my-image:1.0
```

## __Fetch Docker image from Nexus__

__Step 1:__ fetch your image from nexus
```sh
curl -u log:pwd -X GET 'endpoint_of_image?repository=docker-hoasted'
```