## __Demo roject:__
Dockerize Nodejs application and push to private Docker registry
## __Technologies used:__
Docker, Node.js, Amazon ECR
## __Project Decription:__
* Write Dockerfile to build a Docker image for a Nodejs application
* Create private Docker registry on AWS (Amazon ECR)
* Push Docker image to this private repository
## __Description in details:__
### __Write Dockerfile to build a Docker image for a Nodejs application__
__Step 1:__ Write Dockerfile

__Step 2:__ Build a Docker image

### __Create private Docker registry on AWS (Amazon ECR)__
__Step 1:__ Create ECR 

__Step 2:__ Install AWS Cli (use __[this](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)__ official instruction)

__Step 3:__ Configure AWS Cli using __[official instruction](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)__

__Step 4:__ Authenticate to your default registry ([instruction](https://docs.aws.amazon.com/AmazonECR/latest/userguide/getting-started-cli.html#cli-authenticate-registry))
```sh
aws ecr get-login-password --region region | docker login --username AWS --password-stdin aws_account_id.dkr.ecr.region.amazonaws.com
```
### __Push Docker image to this private repository__
__Step 1:__ Tag your image so you can push the image using this syntax : `registryDomain/imageName:tag`  

_NOTE: You can copy  complete commands from UI in "Push commands for my app"_
```sh
docker tag my-app:latest registryDomain/imageName:tag
```
__Step 2:__ Push your image to AWS repository:
```sh
docker push registryDomain/imageName:tag
```
