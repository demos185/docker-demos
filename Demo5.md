## __Demo Project:__
Persist data with Docker Volumes
## __Technologies used:__
Docker, Node.js, MongoDB
## __Project Decription:__
* Persist data of a MongoDB container by attaching a Docker volume to it
## __Description in details:__
### __Persist data of a MongoDB container by attaching a Docker volume to it__
__Step 1:__ Open your docker-compose file with text editor and define list of volume that you're going to use

_Note: add it on services level_
```yaml
volumes:
  mongo-data:
    driver: local
```
__Step 2:__ assign your volume to container
```yaml
  mongodb:
    image: mongo
    ports:
     - 27017:27017
    environment:
     - MONGO_INITDB_ROOT_USERNAME=admin
     - MONGO_INITDB_ROOT_PASSWORD=password
    volumes:
     - mongo-data:/data/db

```
__Step 3:__ Restart your docker-compose
```sh
docker-compose -f docker-compose.yaml down
docker-compose -f docker-compose.yaml up
```
### __Docker Volume Locations__
* Windows: C:/ProgramData/docker/volumes
* Linux: /var/lib/docker/volumes
* Mac: /var/lib/docker/volumes
_Note: Docker for Mac create a Linux virtual machine and stroes all the Docker data_