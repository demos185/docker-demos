## __Demo Project:__
Deploy Nexus as Docker container
## __Technologies used:__
Docker, Nexus, DigitalOcean, Linux
## __Project Decription:__
* Create and Configure Droplet
* Set up and run Nexus as a Docker container
## __Description in details:__
### __Create and Configure Droplet__
__Step 1:__ Create droplet and assign firewall

__Step 2:__ Install docker on droplet
```sh
snap install docker
```

### __Set up and run Nexus as a Docker container__
__Step 1:__  Create volume for Nexus
```sh
docker volume create --name nexus-data
```
__Step 2:__ Run Nexus container
```sh
docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3
```
_Note: You don't have create nexus user because it has already been created!_


